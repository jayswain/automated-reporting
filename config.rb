class Config
  AUTOMATED_REPORTING_REPO_PATH = '/jayswain/automated-reporting'
  AUTO_REPORT_LABEL = 'auto-report'
  DEFAULT_REPO_BRANCH = 'main'
  DRI_UPDATE_EMOJI= 'triangular_flag_on_post'
  DRI_UPDATE_TITLE= 'Async issue update'
  ENGINEERS_REPORT_EPIC_IID = ENV['ENGINEERS_REPORT_EPIC_IID']
  ENGINEERS_REPORT_TITLE_STRING = 'Engineers Report'
  GITLAB_API_TOKEN = ENV['GITLAB_API_PRIVATE_TOKEN']
  GITLAB_COM_HOST = 'gitlab.com'
  GITLAB_ORG_GROUP_PATH = 'gitlab-org'
  GROUP_ENGINEERS = ENV['GROUP_ENGINEERS'].split(' ')
  GROUP_LABEL = ENV['GROUP_LABEL']
  GROUP_NAME = GROUP_LABEL.split(':').last.humanize
  GROUP_PATH = ENV['GROUP_PATH']
  GROUP_PROJECT_PATH = [GROUP_PATH, ENV['GROUP_PROJECT_PATH']].join('/')
  IGNORE_LABEL = 'type::ignore'
  INFRADEV_LABEL = 'infradev'
  OKR_LABEL_REGEX = 'auto-report::OKR::.*'
  OKR_PROJECT_PATH = 'gitlab-com/gitlab-OKRs'
  OPENED_STATE = 'opened'
  PLANNING_LABEL = 'Planning Issue'
  PLANNING_BREAKDOWN_LABEL = 'workflow::planning breakdown'
  PLANNING_TEMPLATE_PATH = '.gitlab/issue_templates/planning_template.md'
  RELEASE_POST_REPORT_TITLE_SEARCH_STRING = 'Release post report'
  SCHEDULING_LABEL = 'workflow::scheduling'
  SECURITY_LABEL = 'security'
  SECTION_NAME = ENV['SECTION_NAME']
  USERNAME = ENV['USERNAME']
  WEEKLY_UPDATE_EPIC_ID = ENV['WEEKLY_UPDATE_EPIC_ID']
  WEEKLY_UPDATE_TITLE_SEARCH_STRING = ENV['WEEKLY_UPDATE_TITLE_SEARCH_STRING']
end