class ProjectPresenter
  include IssueSortable

  attr_accessor :object, :options

  def initialize(object:, options: {})
    @object = object
    @options = options
  end

  def issues
    IssuesFinder.issue_nodes(object.issues.nodes)
  end

  def milestone_start_date
    milestone = milestones.nodes.find do |milestone|
      milestone.title.match?(/^FY\d{2}-Q[1-4]$/)
    end

    milestone.start_date
  end

  def method_missing(m, *args, &block)
    object.send(m, *args, &block)
  end
end
