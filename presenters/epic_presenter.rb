require_relative 'issuable_presenter'

class EpicPresenter
  include IssueSortable
  include IssuablePresenter

  def id_from_gid
    object.id.split('/').last
  end

  def issues
    ## FIXME - if you execute this inline then scopes disappear. ;(
    scope_friendly_options = issue_options

    nodes = IssuesFinder.epic(id_from_gid).call(**scope_friendly_options)
    sort_by_workflow_and_state(nodes.flatten)
  end

  def epics
    child_iids = object.children.nodes.map(&:iid)

    return [] if child_iids.empty?

    EpicIssuesFinder.gitlab_org.call(iids: child_iids)
  end

  def open_issues
    issues.filter {|i| i.state != "closed"}
  end

  def closed_issues
    issues.filter {|i| i.state != "opened"}
  end

  def issue_progress
    states = issues.map(&:state)
    tally = states.tally

    "#{ tally["closed"] || 0 } / #{ states.count } Closed"
  end

  def progress_integer
    states = issues.map(&:state)
    tally = states.tally

    ((tally["closed"].to_f / states.count) * 100).round
  end

  def epic
    return object.epic if object.respond_to?(:epic)

    return object.parent if object.respond_to?(:parent)

    nil
  end

  def health_status
    return '-'
  end

  def assignees
    '-'
  end

  def milestone
    nil
  end

  private

  def sanatized_options
    keys_to_reject = [:ignore_issues_closed_before_date]

    options.reject { |key, _| keys_to_reject.include?(key) }
  end

  def issue_options
    options = sanatized_options

    options[:not_iids] = issues_closed_before_date_iids

    options
  end

  def issues_closed_before_date_iids
    date = options[:ignore_issues_closed_before_date]
    return [] if date.nil?

    IssuesFinder.epic(id_from_gid).closed_before(date).call(**sanatized_options).map(&:iid)
  end

  def dris
    Config::GROUP_ENGINEERS
  end

  def group_path
    path_array = web_path.split("/").reject{|s| s.blank? || s.match?("groups") }
    cutoff_index = path_array.index("-") - 1

    path_array[0..cutoff_index].join('/')
  end
end
