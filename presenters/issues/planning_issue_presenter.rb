module Issues
  class PlanningIssuePresenter
    attr_reader :date

    def initialize(date:)
      @date = date
    end

    def title
      "#{next_milestone_title} #{group_name} - Planning Issue"
    end

    def description
      ## issuable_templates aren't assignable via API, so we copy the
      ## template here and manually set it in the description :clownface:
      blob = project.repository.blobs.nodes.first.raw_blob.dup
      blob.gsub(/MILESTONE/, next_milestone_title)
    end

    def project_gid
      project.id
    end

    private

    def project
      @project ||= ProjectFinder.for_group.date(date).file(Config::PLANNING_TEMPLATE_PATH).call
    end

    def group_name
      [Config::SECTION_NAME, Config::GROUP_NAME].join('::')
    end

    def next_milestone_title
      project.milestones.nodes.filter {|m| m.title.match?(/^\d*.\d*$/) }.first.title
    end
  end
end