class OKRReportPresenter
  extend UrlHelper

  def self.call(okr_label_regex: Config::OKR_LABEL_REGEX, milestone_title: nil)
    project = ProjectFinder.okr.open.date(Date.today.iso8601).call(assignees: [Config::USERNAME])

    project.issues.map do |okr_issue|
      labels = okr_issue.label_names(/#{okr_label_regex}/)

      next if labels.empty?

      closed_issues, open_issues = nil

      if okr_issue.objective?
        closed_issues = okr_issue.child_items_finder.closed_after(project.milestone_start_date).call.issues
        open_issues = okr_issue.child_items_finder.open.call.issues
      else
        closed_issues = IssuesFinder.for_group.milestone(milestone_title).closed_after(project.milestone_start_date).call(label_names: labels, not_iids: okr_issue.iid)
        open_issues = IssuesFinder.for_group.milestone(milestone_title).open.call(label_names: labels, not_iids: okr_issue.iid)
      end

      collection = CollectionPresenter.create(
        open_issues: open_issues,
        closed_issues: closed_issues,
        labels: labels,
        title: okr_issue.title,
        dri_notes: okr_issue.dri_notes
      )

      collections = []
      collections << collection

      collections << EpicIssuesFinder.gitlab_org.for_group.call(label_names: labels, ignore_issues_closed_before_date: project.milestone_start_date, milestone_title: milestone_title) unless labels.empty?

      decorated_collections = collections.flatten.compact.map do |collection|
        OkrCollectionPresenter.create(
          okr_issue: okr_issue,
          collection: collection
        )
      end

      new_pipeline_url = new_pipeline_url(params: { 'OKR_LABEL_REGEX' => labels.first })

      presenter = OpenStruct.new({
        okr_issue: okr_issue,
        new_pipeline_url: new_pipeline_url,
        collections: decorated_collections
      })

    end.compact
  end
end
