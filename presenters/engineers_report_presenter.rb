class EngineersReportPresenter
  def self.call
    @start_date = Date.tomorrow - 1.month
    @end_date = @start_date + 1.month
    context = OpenStruct.new({
      start_date: @start_date,
      end_date: @end_date,
      engineers: engineers,
    })
  end

  def self.engineers
    engineers = UsersFinder.group_engineers.merged_after(@start_date).merged_before(@end_date).call
    engineers.nodes.map do |user|
      merge_requests = user.authored_merge_requests.nodes

      mttms = merge_requests.map do |merge_request|
        if [merge_request.merged_at, merge_request.prepared_at].any?(&:nil?)
          puts 'Merge request has no merged_at or prepared_at'
          next
        end
        (Date.parse(merge_request.merged_at) - Date.parse(merge_request.prepared_at)).to_i
      end.compact

      activities = ContributionEventsFinder.call(user: user.username, before: @end_date, after: @start_date)

      open_issues = IssuesFinder.open.assignees([user.username]).not_labels([Config::IGNORE_LABEL, Config::SCHEDULING_LABEL, Config::PLANNING_BREAKDOWN_LABEL, Config::PLANNING_LABEL]).call
      closed_issues = IssuesFinder.closed_after(1.week.ago.iso8601).assignees([user.username]).not_labels([Config::IGNORE_LABEL, Config::SCHEDULING_LABEL, Config::PLANNING_BREAKDOWN_LABEL, Config::PLANNING_LABEL]).call

      OpenStruct.new({
        username: user.username,
        merge_requests: merge_requests,
        mttm: (mttms.sum(0.0) / mttms.size).round(1),
        mr_rate: (merge_requests.size.to_f / ((@end_date - @start_date).to_f/30)).round(1),
        activities: activities,
        open_issues: open_issues,
        closed_issues: closed_issues
      })
    end
  end
end
