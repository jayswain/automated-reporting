module IssuablePresenter
  attr_accessor :object, :options

  def initialize(object:, options: {})
    @object = object
    @options = options
  end

  def title
    object.title.split(' ').map{|w| w.truncate(30) }.join(' ').gsub('|', '')
  end

  def health_status
    return '**CLOSED**' if object.state == 'closed'

    status = object.health_status

    return '-' if status.to_s.empty?

    "**#{ status.underscore.humanize }**"
  end

  def dri_notes
    return @dri_notes if defined? @dri_notes

    return [] if object.notes.nil?

    ## BUG - this dupes the push
    issuable_dris = dris.push(Config::USERNAME)

    notes = object.notes.nodes.dup

    if epic.present?
      notes.concat(epic.notes.nodes.dup)
    end

    if notes.empty?
      @dri_notes = []
    else
      notes = notes.filter do |note|
        issuable_dris.any?(note.author.username) &&
          (note.body.match?(/^#*\s*#{ Config::DRI_UPDATE_TITLE }/) ||
          note.award_emoji.nodes.map(&:name).any?(Config::DRI_UPDATE_EMOJI))
      end

      @dri_notes = notes.sort_by{|i| i.created_at }.reverse.map do |note|
        OpenStruct.new({
          created_at: Date.parse(note.created_at).strftime('%Y-%m-%d'),
          body: note.body.delete('@').gsub("\n", ' ')
        })
      end
    end
  end

  def label_names(regex)
    object.labels.nodes.map(&:title).select {|label| label =~ regex }
  end

  def labels(regex)
    matches = label_names(regex)
    return [] if matches.empty?

    matches.map {|label| "~\"#{label}\"" }
  end

  def all_labels
    markdown = object.labels.nodes.map do |label|
      "~\"#{label.title}\""
    end
    markdown.join(' ')
  end

  def assignees
    assignees = object.assignees

    return "Unassigned" if assignees.nodes.empty?

    assignees.nodes.map(&:username).join(", ")
  end

  def progress_integer
    return 0 if progress_widget.nil?

    progress_widget.progress
  end

  def method_missing(m, *args, &block)
    object.send(m, *args, &block)
  end

  def work_item
    WorkItemsFinder.call(context_namespace_path: group_path, refs: [object.web_url]).first
  end

  def child_items_finder
    return NullFinder if hierarchy_widget.children.nodes.empty?

    iids = hierarchy_widget.children.nodes.map(&:iid)

    project_path = hierarchy_widget.children.nodes.first.project.full_path

    ProjectFinder.path(project_path).issue_iids(iids)
  end

  def objective?
    type == 'OBJECTIVE'
  end

  def weight
    '-'
  end

  private

  def group_path
    ## FIXME - too fragile

    web_path.split("/").reject{|s| s.blank? }.first
  end

  def widgets
    work_item.widgets
  end

  def progress_widget
    widgets.filter {|w| w.type == "PROGRESS" }.first
  end

  def hierarchy_widget
    widgets.filter {|w| w.type == "HIERARCHY" }.first
  end

  def dris
    object.assignees.nodes.map(&:username)
  end
end
