class CollectionPresenter
  attr_accessor :open_issues, :closed_issues, :labels, :title, :dri_notes

  def self.create(args = {})
    begin
      new(**args)
    rescue ArgumentError
      nil
    end
  end

  def initialize(open_issues: [], closed_issues: [], labels: [], title: '', dri_notes: [])
    @open_issues = open_issues
    @closed_issues = closed_issues
    @labels = labels
    @title = title
    @dri_notes = dri_notes

    raise ArgumentError, "Invalid parameter" if @open_issues.empty? && @closed_issues.empty?
  end

  def due_date
    '-'
  end

  def issue_progress
    "#{ closed_issues.count } / #{ open_issues.count + closed_issues.count } Closed"
  end

  def progress_integer
    ((closed_issues.count.to_f / (open_issues.count + closed_issues.count)) * 100).round
  end

  def web_url
    "https://gitlab.com/dashboard/issues?sort=updated_desc&state=opened&label_name[]=#{Config::GROUP_LABEL}&label_name[]=#{labels.first}"
  end
end
