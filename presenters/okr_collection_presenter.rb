class OkrCollectionPresenter
  attr_accessor :okr_issue, :collection

  def self.create(args = {})
    begin
      new(**args)
    rescue ArgumentError
      nil
    end
  end

  def initialize(okr_issue:, collection:)
    @okr_issue = okr_issue
    @collection = collection
  end

  def progress_integer
    [okr_issue.progress_integer, collection.progress_integer].compact.max
  end

  def method_missing(m, *args, &block)
    collection.send(m, *args, &block)
  end
end
