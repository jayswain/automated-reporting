class ProjectFinder < BaseFinder
  def initialize(path: nil, date: nil, branch_name: nil, file_path: [], work_item_id: '', assignees: nil, issue_iids: [], issue_state: nil)
    @query_class = ::GraphQL::Queries::ProjectQuery

    @variables = {
      path: path,
      date: date,
      branchName: branch_name,
      filePath: file_path,
      workItemId: work_item_id,
      assignees: assignees,
      issueIids: issue_iids,
      issueState: issue_state,
    }
  end

  def self.date(date)
    scope_variables[:date] = date

    return self
  end

  def self.file(file_path)
    scope_variables[:branchName] = Config::DEFAULT_REPO_BRANCH
    scope_variables[:filePath] = file_path

    return self
  end

  def self.for_group
    scope_variables[:path] = Config::GROUP_PROJECT_PATH

    return self
  end

  def self.okr
    scope_variables[:path] = Config::OKR_PROJECT_PATH

    return self
  end

  def self.path(path)
    scope_variables[:path] = path

    return self
  end

  def self.issue_iids(iids)
    scope_variables[:issueIids] = iids

    return self
  end

  def self.open
    scope_variables[:issueState] = Config::OPENED_STATE

    return self
  end

  def self.closed_after(date)
    scope_variables[:issueClosedAfter] = date

    return self
  end

  def nodes
    ProjectPresenter.new(object: results.project)
  end
end