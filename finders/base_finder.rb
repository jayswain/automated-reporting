class BaseFinder
  attr_accessor :label_names, :query_class, :variables

  SCOPE_VARIABLES_DEFAULT = {}

  def self.call(args = {})
    instance = new(**args)

    return instance.nodes
  end

  def results
    merged_variables = variables.deep_merge(self.class.scope_variables)

    results = query_class.new(variables: merged_variables).results

    raise results.original_hash.to_s unless results.errors.empty?

    data = results.data

    self.class.clear_scope_variables!

    data
  end

  def self.clear_scope_variables!
    @@scope_variables = nil
  end

  def self.scope_variables
    return @@scope_variables if self.class_variable_defined?(:@@scope_variables) && !!@@scope_variables

    @@scope_variables = Marshal.load(Marshal.dump(self::SCOPE_VARIABLES_DEFAULT))
  end
end