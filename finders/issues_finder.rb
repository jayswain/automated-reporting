class IssuesFinder < BaseFinder
  extend IssueSortable
  extend IssuableScopes

  SCOPE_VARIABLES_DEFAULT = { labelNames: [] }

  def initialize(state: nil, label_names: [], closed_after: nil, closed_before: nil, group_label_name: nil, epic_id: nil, search: nil, search_in: [], milestone_title: nil, not_labels: [], not_iids: [], iids: [])
    @query_class = ::GraphQL::Queries::IssuesQuery

    @variables = {
      state: state,
      groupLabelName: group_label_name,
      labelNames: label_names,
      closedAfter: closed_after,
      closedBefore: closed_before,
      epicId: epic_id,
      search: search,
      in: search_in,
      milestoneTitle: milestone_title,
      notLabels: not_labels,
      notIids: not_iids,
      iids: iids
    }
  end

  def nodes
    self.class.issue_nodes(results.issues.nodes)
  end

  def self.issue_nodes(nodes)
    nodes = nodes.map do |issue_fragment|
      issue = GraphQL::Queries::IssuesQuery::NODE_FRAGMENT.new(issue_fragment)
      IssuePresenter.new(object: issue)
    end
    sort_by_workflow_and_state(nodes)
  end
end
