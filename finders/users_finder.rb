class UsersFinder < BaseFinder
  def initialize(usernames: [], created_after: nil, created_before: nil, state: nil, merged_after: nil, merged_before: nil, sort: 'MERGED_AT_DESC')
    @query_class = ::GraphQL::Queries::UsersQuery

    @variables = {
      usernames: usernames,
      createdAfter: created_after,
      createdBefore: created_before,
      state: state,
      mergedAfter: merged_after,
      mergedBefore: merged_before,
      sort: sort
    }
  end

  def self.group_engineers
    scope_variables[:usernames] = Config::GROUP_ENGINEERS

    return self
  end

  def self.created_after(date)
    scope_variables[:createdAfter] = date

    return self
  end

  def self.created_before(date)
    scope_variables[:createdBefore] = date

    return self
  end

  def self.closed
    scope_variables[:state] = 'closed'

    return self
  end

  def self.merged
    scope_variables[:state] = 'merged'

    return self
  end

  def self.opened
    scope_variables[:state] = 'opened'

    return self
  end

  def self.merged_after(date)
    scope_variables[:mergedAfter] = date

    return self
  end

  def self.merged_before(date)
    scope_variables[:mergedBefore] = date

    return self
  end

  def nodes
    results.users
  end
end
