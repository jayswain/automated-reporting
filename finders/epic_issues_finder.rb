class EpicIssuesFinder < BaseFinder
  extend IssuableScopes

  SCOPE_VARIABLES_DEFAULT = { labelNames: [] }

  def initialize(label_names: nil, milestone_title: nil, iids: [], ignore_issues_closed_before_date: nil)
    @query_class = ::GraphQL::Queries::EpicIssuesQuery

    @variables = {
      labelNames: [label_names].flatten,
      childIids: iids
    }

    @nodes_options = {
      milestone_title: milestone_title,
      ignore_issues_closed_before_date: ignore_issues_closed_before_date,
     }
  end

  def self.engineers
    self.scope_variables[:groupPath] = Config::GROUP_PATH
    self.scope_variables[:iids] = Config::ENGINEERS_REPORT_EPIC_IID

    return self
  end

  def self.gitlab_org
    self.scope_variables[:groupPath] = Config::GITLAB_ORG_GROUP_PATH

    return self
  end

  def self.for_group
    # Epic issue "or" filtering is broken in the Epic GraphQL API,
    # so we're passing labels together here instead of using groupLabelName
    self.scope_variables[:labelNames].push(Config::GROUP_LABEL)

    return self
  end

  def nodes
    epics = results.group.epics.nodes

    epics.map do |epic|
      EpicPresenter.new(object: epic, options: @nodes_options)
    end
  end
end