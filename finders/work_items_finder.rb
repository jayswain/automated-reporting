class WorkItemsFinder < BaseFinder
  def initialize(context_namespace_path: nil, refs: [])
    @query_class = ::GraphQL::Queries::WorkItemsQuery

    @variables = {
      contextNamespacePath: context_namespace_path,
      refs: refs
    }
  end

  def nodes
    results.work_items_by_reference.nodes
  end
end
