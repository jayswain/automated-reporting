require 'httparty'

class ContributionEventsFinder
  API_BASE_URL = "https://gitlab.com/api/v4/users/:id/events"

  attr_accessor :activities, :args, :user

  def self.call(args = {})
    default_args = {
      page: 1,
      per_page: 50
    }

    args[:before] = args[:before].strftime("%Y/%m/%d") if args[:before]
    args[:after] = args[:after].strftime("%Y/%m/%d") if args[:after]

    instance = new(args.extract!(:user)[:user], default_args.merge(args))

    instance.activities
  end

  def initialize(user, args)
    @user = user
    @args = args

    @activities = get_all_activities
  end

  def get_all_activities
    url = API_BASE_URL.gsub(":id", user)
    activities = []

    while true
      response = make_request(url, args)

      if response.success?
        activities.push(JSON.parse(response.body))
        args[:page] += 1
      else
        puts "Error finding events for user: #{response.code} - #{response.body}"
        break
      end
      break if args[:page] == 100
      break if response.empty?
    end

    activities.flatten.group_by {|a| a['created_at'].split('T').first }
  end

  def make_request(url, query)
    HTTParty.get(
      url,
      query: query,
      headers: {
        "Content-Type": "application/json",
        "PRIVATE-TOKEN": Config::GITLAB_API_TOKEN
      }
    )
  end
end
