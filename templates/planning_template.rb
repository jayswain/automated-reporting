class PlanningTemplate
  extend ActionView::Helpers::TextHelper
  extend ApplicationHelper

  def self.render(nodes:, options: {})
    markdown = <<~MARKDOWN

    ## #{options[:milestone_title]} OKR related issues

    Below you can find issues related to our [Team OKRs](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Aauthorization&first_page_size=100)
    that are slotted for the #{options[:milestone_title]} milestone. They're sorted by workflow and priority.
    MARKDOWN

    puts markdown
    puts ''
    puts ''

    nodes.each do |presenter|
      next if presenter.okr_issue.objective?

      presenter.collections.each do |epic|
        self.render_epic_issues(epic: epic)
        puts ""
        puts ""
      end
    end


    puts ''
    generated_by_signature(secret_url: options[:new_pipeline_url])
  end

  def self.render_epic_issues(epic:)
    puts "[#{epic.title}](#{ epic.web_url })"
    puts "---"
    puts ""

    puts '| Title | State | Assignee | Weight | Labels |'
    puts '| --- | --- | --- | --- | --- |'
    epic.open_issues.each do |issue|
      puts "| [#{ issue.title }](#{ issue.web_url }) | #{ issue.state } | #{ issue.assignees } | #{ issue.weight } | #{ issue.all_labels } |"
    end

    puts ''
    puts ''
    markdown = <<~MARKDOWN
    <!-- GLQL FRIENDLY MARKDOWN FOR COPY PASTA
      ```glql
      ---
      display: table
      fields: title, state, assignee, weight, labels
      ---
      group = "gitlab-org"
      AND id in (#{ epic.open_issues.map(&:iid).join(',') })
      ```
    -->
    MARKDOWN

    puts markdown
  end
end