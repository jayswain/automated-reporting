class IssueTableTemplate
  extend IssuableTableHelper

  def self.render(nodes:, options: {})
    return if nodes.empty?

    @options = options

    puts '| Priority | Current Status | Progress | Milestone/Due Date |'
    puts '| --- | --- | --- | --- |'

    nodes.each do |issue|
      puts row(issue)
    end
    puts ''
  end

  private

  def self.row(issue)
    template_row = <<~MARKDOWN
    [#{ issue.title }](#{ issue.web_url })  | #{ [issue.health_status.upcase, issue.labels(/workflow|priority|severity/), issue.assignees].flatten.join(' <br /> ') } | #{ dri_notes_column(issue.dri_notes, show_all: @options[:show_all_dri_notes]) } | #{ [issue.milestone&.title, issue.due_date].compact.join(' - ') } |
    MARKDOWN
  end
end
