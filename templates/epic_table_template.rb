class EpicTableTemplate
  extend ActionView::Helpers::TextHelper
  extend IssuableTableHelper

  def self.render(nodes:)
    return if nodes.empty?

    nodes.each do |epic|
      puts '| Priority | Current Status | Progress | Due Date |'
      puts '| --- | --- | --- | --- |'
      puts "| [#{epic.title}](#{ epic.web_url }) | #{ dri_notes_column(epic.dri_notes) } | ![Progress Bar at #{ epic.progress_integer }%](https://geps.dev/progress/#{ epic.progress_integer } '#{ epic.issue_progress }') | #{ epic.due_date } |"

      self.render_epic_issues(epic: epic)
    end
  end

  def self.render_epic_issues(epic:)
    puts '<details>'
    puts "<summary> Show issue details for [#{ truncate(epic.title, length: 85) }](#{ epic.web_url }) </summary>"
    puts ''

    IssueTableTemplate.render(nodes: epic.open_issues)

    puts '</details>'
    puts ''

    if epic.closed_issues.count > 0
      puts '<details>'
      puts "<summary> Show all #{ epic.closed_issues.count } issues closed </summary>"
      puts ''

      IssueTableTemplate.render(nodes: epic.closed_issues)

      puts '</details>'
      puts ''
    end
  end
end