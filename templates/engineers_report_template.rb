class EngineersReportTemplate
  extend ActionView::Helpers::DateHelper
  extend ApplicationHelper

  def self.render(context)
    puts  "# Engineering Report"

    time_ago = distance_of_time_in_words(context.start_date, context.end_date)

    context.engineers.each do |engineer|
      puts '<details>'
      puts "<summary> #{ engineer.username } - Show details </summary>"
      puts ''

      puts  "## Merge requests merged from #{ context.start_date.strftime("%B %d") } to #{ context.end_date.strftime("%B %d") } (#{ time_ago })"
      puts '| Engineer | MR Rate | MTTM |'
      puts '| --- | --- | --- |'
      puts "| #{ engineer.username } | #{ engineer.mr_rate } | #{ engineer.mttm } |"

      puts '<details>'
      puts "<summary> Show all merged merge requests </summary>"
      puts ''

      puts '| Title | Merged At |'
      puts '| --- | --- |'
      engineer.merge_requests.each do |merge_request|
        puts "| #{ merge_request.web_url }+ | #{ merge_request.merged_at } |"
      end

      puts '</details>'
      puts ''

      puts '<details>'
      puts "<summary> Show all contribution activity </summary>"
      puts ''

      puts '| Date | Contribution times |'
      puts '| --- | --- |'
      engineer.activities.each do |activity_date, activity|
        puts "#{ activity_date } #{ Date.parse(activity_date).strftime("%A") } | #{ activity.reverse.map {|a| "#{DateTime.parse(a['created_at']).strftime('%H:%M:%S')} - #{a['action_name']} #{a['target_title']&.truncate(20)}" }.join(' <br /> ') }"
      end

      puts '</details>'
      puts ''

      puts '## Issues'

      IssueTableTemplate.render(nodes: engineer.open_issues, options: { show_all_dri_notes: true })

      puts '<details>'
      puts "<summary> Show all #{ engineer.closed_issues.count } Issues closed in the last week </summary>"
      puts ''

      IssueTableTemplate.render(nodes: engineer.closed_issues, options: { show_all_dri_notes: true })

      puts '</details>'
      puts ''

      puts '</details>'
      puts ''
    end

    puts ''
    generated_by_signature
    puts ''
    puts "/confidential"
    puts "/label ~#{ Config::GROUP_LABEL } ~#{ Config::IGNORE_LABEL}"
  end
end
