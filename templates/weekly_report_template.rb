class WeeklyReportTemplate
  extend ApplicationHelper
  extend IssuableTableHelper

  def self.render(presenters)
    return if presenters.empty?

    puts "### #{Config::GROUP_NAME} Priorities"
    puts ''

    puts '| Priority | Current Status | Progress | Due Date |'
    puts '| --- | --- | --- | --- |'

    presenters.each do |presenter|
      presenter.collections.each do |epic|
        puts "| [#{presenter.okr_issue.title}](#{ presenter.okr_issue.web_url }) | #{ dri_notes_column(epic.dri_notes) } | ![Progress Bar at #{ epic.progress_integer }%](https://geps.dev/progress/#{ epic.progress_integer } '#{ epic.issue_progress }') | #{ epic.due_date } |"
      end
    end

    puts ''
    generated_by_signature

    doc_friendly_markdown = ''
    presenters.each do |presenter|
      presenter.collections.each do |epic|
        markdown = <<~MARKDOWN
          * [#{presenter.okr_issue.title}](#{ presenter.okr_issue.web_url })
              * **Progress**: #{ epic.progress_integer }% - #{ epic.issue_progress }
              * **Status**: #{ dri_notes_column(epic.dri_notes) }

        MARKDOWN
        doc_friendly_markdown << markdown
      end
    end

    comment = <<~HTML
      <!-- GOOGLE DOC FRIENDLY MARKDOWN FOR COPY PASTA
        #{ doc_friendly_markdown }
      -->
    HTML

    puts
    puts comment
  end
end