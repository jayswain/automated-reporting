class OKRReportTemplate
  extend ApplicationHelper

  def self.render(context)
    @context = context

    puts "<!-- #{ @context.okr_issue.title }: #{ @context.okr_issue.web_url } -->"

    EpicTableTemplate.render(nodes: @context.collections)

    generated_by_signature(secret_url: @context.new_pipeline_url)
  end
end
