module IssuableTableHelper
  def dri_notes_column(notes, show_all: false)
    notes = notes.first(1) unless show_all

    formatted_notes = notes.compact.map do |note|
      [note.created_at, note.body].join(' - ') || '-'
    end

    formatted_notes&.join(' <br /> <br />')
  end
end