require 'uri'

module UrlHelper
  NEW_PIPELINE_PATH = "#{ Config::AUTOMATED_REPORTING_REPO_PATH }/-/pipelines/new"
  AUTOMATED_REPORTING_REPO_URL = ::URI::HTTPS.build(:host => Config::GITLAB_COM_HOST, path: Config::AUTOMATED_REPORTING_REPO_PATH).to_s

  def new_pipeline_url(params: {})
    gitlab_ci_only_param = Rake.application.top_level_tasks.first.split(':').push('job').join('_').upcase
    params[gitlab_ci_only_param] = 'true'
    url = URI::HTTPS.build(:host => Config::GITLAB_COM_HOST, path: NEW_PIPELINE_PATH, query: { var: params  }.to_query).to_s
  end
end