require './app'

namespace :engineers do
  desc "Generate a markdown report of GROUP_ENGINEERS, including their current MR Rate, MTTM and closed MR links"
  task :report do
    presenter = EngineersReportPresenter.call

    EngineersReportTemplate.render(presenter)
  end

  namespace :issues do
    desc 'Update the latest Engineers Report issue with rake:engineers:report'
    task :update do
      epic = EpicIssuesFinder.engineers.call.first
      issue = IssuesFinder.epic(epic.id_from_gid).
        title(Config::ENGINEERS_REPORT_TITLE_STRING).open.call.first

      body = with_captured_stdout do
        Rake::Task["engineers:report"].invoke
      end

      UpdateIssueDescriptionService.call(issue.project_id.to_s, issue.iid.to_s, body)
    end

    desc 'Close all issues that have the ENGINEERS_REPORT_TITLE_STRING, that are in the ENGINEERS_REPORT_EPIC_IID.'
    task :close_all do
      epic = EpicIssuesFinder.engineers.call.first
      issues = IssuesFinder.epic(epic.id_from_gid).
        title(Config::ENGINEERS_REPORT_TITLE_STRING).open.call

      UpdateIssueService.call(
        parentId: ProjectFinder.for_group.call.id,
        ids: issues.map(&:id),
        stateEvent: 'CLOSE'
      )
    end

    desc 'Runs rake:engineers:issues:close_all and posts content from rake:engineers:report in new issue under ENGINEERS_REPORT_EPIC_IID'
    task :create do
      Rake::Task["engineers:issues:close_all"].invoke

      body = with_captured_stdout do
        Rake::Task["engineers:report"].invoke
      end

      CreateIssueService.call(
        title: "#{ Config::ENGINEERS_REPORT_TITLE_STRING} - #{ Date.today.strftime('%Y-%m-%d') }",
        description: body,
        epic_work_item_id: EpicIssuesFinder.engineers.call.first.work_item.id
      )
    end
  end
end