require './app'

namespace :okr do
  desc "Generate a markdown report including issues and epics that have the OKR_LABEL and GROUP_LABEL"
  task :report do
    options = {
      okr_label_regex: ENV['OKR_LABEL_REGEX'],
      milestone_title: ENV['MILESTONE_TITLE']
    }.compact

    presenters = OKRReportPresenter.call(**options)

    presenters.each do |presenter|
      OKRReportTemplate.render(presenter)
    end
  end

  namespace :issues do
    desc 'Update all assigned OKR issues with rake:okr:report'
    task :update do
      ## Allow passing in options via command line
      ## Example: rake okr:issues:update OKR_LABEL_REGEX=auto-report::OKR::5
      options = {
        okr_label_regex: ENV['OKR_LABEL_REGEX']
      }.compact

      presenters = OKRReportPresenter.call(**options)

      presenters.each do |presenter|
        body = with_captured_stdout do
          OKRReportTemplate.render(presenter)
        end

        CreateDiscussionService.call(
          body: body,
          noteableId: presenter.okr_issue.id
        )
      end
    end
  end
end
