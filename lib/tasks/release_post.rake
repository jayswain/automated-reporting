require './app'

desc 'Adds a note to the RELEASE_POST_REPORT_TITLE_SEARCH_STRING issue that has the Config::IGNORE_LABEL with the report:release_post output'
namespace :release_post do
  desc "Generate a markdown report of issues that have the GROUP_LABEL that were closed in the current milestone of the GROUP_PROJECT_PATH"
  task :report do
    project ||= ProjectFinder.for_group.date(Date.today).call

    current_milestone = project.milestones.nodes.first

    issues_closed_in_last_milestone = IssuesFinder.for_group.closed_after(current_milestone.start_date).call

    sorted = issues_closed_in_last_milestone.sort_by{|i| [i.epic&.title || '', i.closed_at]}

    ReleasePostTemplate.render(nodes: sorted)
  end

  namespace :issues do
    task :comment do
      release_post_issues = IssuesFinder.open.ignore.for_group.release_post.call

      return unless release_post_issues.count == 1

      body = with_captured_stdout do
        Rake::Task["release_post:report"].invoke
      end

      CreateDiscussionService.call(
        body: body,
        noteableId: release_post_issues.first.id
      )
    end
  end
end
