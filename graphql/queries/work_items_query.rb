module GraphQL
  module Queries
    class WorkItemsQuery < GraphQL::BaseClient
      QUERY = GraphQL::BaseClient::Client.parse <<-'GRAPHQL'
        query ($contextNamespacePath: ID, $refs: [String!]!) {
          workItemsByReference(contextNamespacePath: $contextNamespacePath, refs: $refs) {
            nodes {
              title
              id
              iid
              widgets {
                type
                ... on WorkItemWidgetProgress {
                  progress
                }
                ... on WorkItemWidgetHierarchy {
                  children {
                    nodes {
                      iid
                      title
                      project {
                        fullPath
                      }
                    }
                  }
                }
              }
            }
          }
        }
      GRAPHQL
    end
  end
end
