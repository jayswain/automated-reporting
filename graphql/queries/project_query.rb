module GraphQL
  module Queries
    class ProjectQuery < GraphQL::BaseClient
      QUERY = GraphQL::BaseClient::Client.parse <<-'GRAPHQL'
        query ($path:	ID!, $date: Time, $filePath: [String!]!, $branchName: String, $workItemId: String!, $assignees: [String!], $issueIids: [String!], $issueState: IssuableState, $issueClosedAfter: Time) {
          project(fullPath: $path) {
            fullPath
            namespace {
              fullPath
              workItemTypes {
                nodes {
                  id
                  name
                }
              }
              workItem(iid: $workItemId) {
                id
                iid
                webUrl
                title
              }
            }
            id,
            issues(assigneeUsernames: $assignees, state: $issueState, iids: $issueIids, closedAfter: $issueClosedAfter) {
              nodes {
                ...GraphQL::Queries::IssuesQuery::NODE_FRAGMENT
              }
            }
            milestones(includeAncestors: true, state: active, containingDate: $date) {
              nodes {
                title
                startDate
              }
            },
            repository {
              blobs(ref:$branchName, paths: $filePath) {
                nodes {
                  rawBlob
                }
              }
            }
          }
        }
      GRAPHQL
    end
  end
end