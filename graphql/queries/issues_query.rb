module GraphQL
  module Queries
    class IssuesQuery < GraphQL::BaseClient
      NODE_FRAGMENT = GraphQL::BaseClient::Client.parse <<-'GRAPHQL'
        fragment on Issue {
          id
          iid
          projectId
          createdAt
          closedAt
          webUrl
          webPath
          state
          healthStatus
          title
          type
          epic {
            title
            notes(last: 10, filter: ONLY_COMMENTS) {
              nodes {
                awardEmoji {
                  nodes {
                    name
                  }
                }
                author {
                  username
                }
                body
                createdAt
              }
            }
          }
          milestone {
            title
          }
          dueDate
          assignees {
            nodes {
              username
            }
          }
          labels {
            nodes {
              title
            }
          }
          notes(last: 10, filter: ONLY_COMMENTS) {
            nodes {
              awardEmoji {
                nodes {
                  name
                }
              }
              author {
                username
              }
              body
              createdAt
            }
          }
        }
      GRAPHQL

      QUERY = GraphQL::BaseClient::Client.parse <<-'GRAPHQL'
        query ($state: IssuableState, $groupLabelName: [String], $labelNames: [String!], $closedAfter: Time, $closedBefore: Time, $epicId: String, $search: String, $in: [IssuableSearchableField!], $milestoneTitle: [String], $assigneeUsernames: [String!], $notLabels: [String!], $iids: [String!], $notIids: [String!]) {
          issues(state: $state, closedAfter: $closedAfter, closedBefore: $closedBefore, labelName: $groupLabelName, or: { labelNames: $labelNames }, epicId: $epicId, search: $search, in: $in, milestoneTitle: $milestoneTitle, assigneeUsernames: $assigneeUsernames, not: { labelName: $notLabels, iids: $notIids }, iids: $iids, includeSubepics: true) {
            nodes {
              ...GraphQL::Queries::IssuesQuery::NODE_FRAGMENT
            }
          }
        }
      GRAPHQL
    end
  end
end
