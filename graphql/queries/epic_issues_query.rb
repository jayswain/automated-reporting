module GraphQL
  module Queries
    class EpicIssuesQuery < GraphQL::BaseClient
      QUERY = GraphQL::BaseClient::Client.parse <<-GRAPHQL
        query ($labelNames: [String!], $state: EpicState, $childIids: [ID!], $groupPath: ID!) {
          group(fullPath: $groupPath) {
            epics(labelName: $labelNames, state: $state, iids: $childIids) {
              nodes {
                id
                iid
                webUrl
                webPath
                title
                healthStatus
                dueDate
                state
                labels {
                  nodes {
                    title
                  }
                }
                children {
                  nodes {
                    iid
                  }
                }
                notes(last: 10, filter: ONLY_COMMENTS) {
                  nodes {
                    awardEmoji {
                      nodes {
                        name
                      }
                    }
                    author {
                      username
                    }
                    body
                    createdAt
                  }
                }
                parent {
                  title
                  notes(last: 10, filter: ONLY_COMMENTS) {
                    nodes {
                      awardEmoji {
                        nodes {
                          name
                        }
                      }
                      author {
                        username
                      }
                      body
                      createdAt
                    }
                  }
                }
              }
            }
          }
        }
      GRAPHQL
    end
  end
end