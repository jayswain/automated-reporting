module GraphQL
  module Mutations
    class DiscussionMutation < GraphQL::BaseClient
      QUERY = GraphQL::BaseClient::Client.parse <<-'GRAPHQL'
        mutation($body: String!, $noteableId: NoteableID!) {
          createDiscussion(input: { body: $body, noteableId: $noteableId }) {
            errors
            note {
              url
            }
          }
        }
      GRAPHQL
    end
  end
end