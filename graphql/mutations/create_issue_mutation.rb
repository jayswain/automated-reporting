module GraphQL
  module Mutations
    class CreateIssueMutation < GraphQL::BaseClient
      QUERY = GraphQL::BaseClient::Client.parse <<-'GRAPHQL'
        mutation($description: String!, $title: String!, $projectPath: ID!, $epicGlobalWorkItemId: WorkItemID, $issueTypeId: WorkItemsTypeID!) {
          workItemCreate(input: {
            workItemTypeId: $issueTypeId,
            descriptionWidget: {
              description: $description
            },
            confidential: true,
            title: $title,
            namespacePath: $projectPath,
            hierarchyWidget: {
              parentId: $epicGlobalWorkItemId
            }
          }) {
            errors
            workItem {
              webUrl
            }
          }
        }
      GRAPHQL
    end
  end
end