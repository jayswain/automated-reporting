## Automated Reporting

This project is designed to automate simple reocurring tasks for Engineering managers. Such as:
  * Report on issues with specific (and configurable) labels, and post comments on desired reporting issues.
  * Stay informed on your team and their veolcity.

## Automations

All automations are ran via rake task. Check out `rake -D` for an updated list.

### Weekly Updates

Keep track of issues and epics that have the `GROUP_LABEL`, and either the `AUTO_REPORT_LABEL`, `SECURITY_LABEL` or `INFRADEV_LABEL`. Pull in the latest DRI comment via emoji. Post a nicely formatted comment to the most recent weekly report issue in a specific epic. [See an example.](https://gitlab.com/gitlab-com/govern-sub-department/-/issues/264#note_1964972654)

### Planning issues

Create a new planning issue using an issue template in your group project and close all previous planning issues for your group.

### OKRs

Keep track of issues and epics that have the `GROUP_LABEL` and the `OKR_LABEL`. Have multiple OKRs? use different OKR labels (~"OKR::1", ~"OKR::2") and track them separately. [See an example](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/7575).

**Note** - Sub-epics are not included and require an `OKR_LABEL` as well.

### Release Posts

Generate a list of issues that have the `GROUP_LABEL` that were closed in the current milestone. Post that list to a specific issue to make collaboration with your Product counterpart easier.

### Engineers

Keep track of `GROUP_ENGINEERS`, including their current MR Rate, MTTM and closed MR links. [See an example.](https://gitlab.com/gitlab-org/govern/authorization/team-tasks/-/issues/41)

## Configuration (local)

1. Copy .env.local.template => .env.local
2. Set values in .env.local
3. Run a task

## Configuration (CI)

1. Set values in [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui) from .env.local
2. Schedule new pipeline via [Scheduled Pipelines](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
3. Target desired job by setting the associated [only variable](https://docs.gitlab.com/ee/ci/yaml/index.html#onlyvariables--exceptvariables) in scheduled pipeline ([see .gitlab-ci.yml](https://gitlab.com/jayswain/automated-reporting/-/blob/main/.gitlab-ci.yml))

**A note on confidentiality**: Jobs can potentially expose private data. If you're running this as a public project, you'll want to disable the [public pipelines setting](https://docs.gitlab.com/ee/ci/pipelines/settings.html#change-which-users-can-view-your-pipelines) for your project.

### Finding Global ID's

Finding Global id's for resources can be tricky, when in doubt, just ask Duo Chat for the global ID:
> give me the full global id with the "gid" string, please.

## Debugging

* `require 'awesome_print'`
* `require 'pry'; binding.pry;`
