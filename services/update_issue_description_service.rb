require 'httparty'

class UpdateIssueDescriptionService
  API_BASE_URL = "https://gitlab.com/api/v4/projects/:id/issues/:issue_iid"

  def self.call(project_id, issue_iid, description)
    url = API_BASE_URL.gsub(":id", project_id).gsub(":issue_iid", issue_iid)

    args = {
      "description": description
    }

    response = make_request(url, args)

    if response.success?
      parsed = JSON.parse(response.body)
      puts parsed['web_url']
    else
      puts "Error updating issue description: #{response.code} - #{response.body}"
    end
  end

  private

  def self.make_request(url, body)
    HTTParty.put(
      url,
      body: body.to_json,
      headers: {
        "Content-Type": "application/json",
        "PRIVATE-TOKEN": Config::GITLAB_API_TOKEN
      }
    )
  end
end
