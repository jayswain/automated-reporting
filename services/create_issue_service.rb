class CreateIssueService
  def self.call(title:, description:, project: ProjectFinder.for_group.call, epic_work_item_id: nil)
    issue_type_id = project.namespace.work_item_types.nodes.find{|w| w.name == 'Issue' }.id

    args = {
      title: title,
      description: description,
      projectPath: project.full_path,
      epicGlobalWorkItemId: epic_work_item_id,
      issueTypeId: issue_type_id,
    }

    result = GraphQL::Mutations::CreateIssueMutation.new(variables: args).results

    puts result.to_h
  end
end