require 'dotenv/load'
Dotenv.load('.env.local')

require 'active_support'
require 'active_support/inflector'
require 'active_support/core_ext/numeric/time'
require 'active_support/core_ext/integer/time'
require 'action_view'
require 'action_view/helpers'
require './config'
Dir[File.join(__dir__, 'lib/', '*.rb')].each { |file| require file }
Dir[File.join(__dir__, 'concerns/**/', '*.rb')].each { |file| require file }
Dir[File.join(__dir__, 'helpers/**/', '*.rb')].each { |file| require file }
Dir[File.join(__dir__, 'presenters/**/', '*.rb')].each { |file| require file }
Dir[File.join(__dir__, 'templates/**/', '*.rb')].each { |file| require file }
Dir[File.join(__dir__, 'graphql/**/', '*.rb')].each { |file| require file }
Dir[File.join(__dir__, 'finders/**/', '*.rb')].each { |file| require file }
Dir[File.join(__dir__, 'services/**/', '*.rb')].each { |file| require file }